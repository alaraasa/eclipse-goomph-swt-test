plugins {
    id("org.standardout.bnd-platform")
    id("com.diffplug.eclipse.mavencentral")
}

dependencies {
    implementation("commons-io:commons-io:2.11.0")

}

eclipseMavenCentral {
    release("4.9.0") {
        implementation("org.eclipse.platform")
        implementation("org.eclipse.jface")
        implementation("org.eclipse.core.resources")
        implementation("org.eclipse.ui")
        constrainTransitivesToThisRelease()
        useNativesForRunningPlatform()
    }
}

osgiBndManifest {
    copyTo("src/main/resources/META-INF/MANIFEST.MF")
    mergeWithExisting(true)
}

group = "org.example"
version = "1.0"

tasks.jar {
    this.manifest {
        attributes["-removeheaders"] = "Bnd-LastModified,Cached-ModelsDir"
    }
}

afterEvaluate {
    this.tasks.findByName("bundles")?.dependsOn(this.tasks.build.get())
}


apply(from = "platform-config.gradle")