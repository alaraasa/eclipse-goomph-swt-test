pluginManagement {
    repositories {
        mavenLocal()
        mavenCentral()
        gradlePluginPortal()
        val danfossArtifactoryUrl: String by settings
        val danfossArtifactoryUser: String by settings
        val danfossArtifactorySecret: String by settings

        maven {
            url = uri("$danfossArtifactoryUrl/zip/VLTDrives/pfpd")
            credentials {
                username = danfossArtifactoryUser
                password = danfossArtifactorySecret
            }
        }
    }
}

//Copy-paste into buildSrc due to no easy way to reuse this
dependencyResolutionManagement {
    versionCatalogs {
        create("libs") {
            library("apache.compress", "org.apache.commons:commons-compress:1.21")
            library("apache.lang3", "org.apache.commons:commons-lang3:3.12.0")
            library("apache.exec", "org.apache.commons:commons-exec:1.3")
            library("apache.ant", "org.apache.ant:com.springsource.org.apache.tools.ant:1.7.0")
            library("apache.commons.io", "commons-io:commons-io:2.11.0")
            library("apache.commons.net", "commons-net:commons-net:3.8.0")
            library("apache.commons.collections", "commons-collections:commons-collections:3.2.2")
            library("apache.commons.collections4", "org.apache.commons:commons-collections4:4.4")
            library("apache.commons.text", "org.apache.commons:commons-text:1.9")

            library("eclipse.persistence", "org.eclipse.persistence:eclipselink:3.0.2")
            library("eclipse.persistence.moxy", "org.eclipse.persistence:org.eclipse.persistence.moxy:3.0.2")

            library("freemarker", "org.freemarker:freemarker:2.3.31")
            library("junit-bom", "org.junit:junit-bom:5.8.2")
            library("junit-jupiter-api", "org.junit.jupiter:junit-jupiter-api:5.8.2")
            library("junit-jupiter-engine", "org.junit.jupiter:junit-jupiter-engine:5.8.2")
            library("junit-vintage-engine", "org.junit.vintage:junit-vintage-engine:5.8.2")
            library("log4j", "log4j:log4j:1.2.17")
            library("easymock", "org.easymock:easymock:4.3")
            library("assertj-core", "org.assertj:assertj-core:3.21.0")

            library("jetbrains.kotlin-stdlib", "org.jetbrains.kotlin:kotlin-stdlib:1.6.0")
            library("jetbrains.kotlin-stdlib.jdk8", "org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.6.0")
            library("jetbrains.kotlin.reflect", "org.jetbrains.kotlin:kotlin-reflect:1.7.0")
            library("jetbrains.kotlinx-coroutines-core", "org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.3")

            library("slf4j-api", "org.slf4j:slf4j-api:1.7.30")
            library("slf4j-log4j12", "org.slf4j:slf4j-log4j12:1.7.30")

            library("tukaani.xz", "org.tukaani:xz:1.9")
            library("jsch", "com.jcraft:jsch:0.1.55")
            library(
                "artifactory-java-client-services",
                "org.jfrog.artifactory.client:artifactory-java-client-services:2.11.1"
            )

            library("graphstream.core", "org.graphstream:gs-core:2.0")
            library("graphstream.algo", "org.graphstream:gs-algo:2.0")
            library("graphstream.ui", "org.graphstream:gs-ui:1.3")
            library("graphstream.mbox2", "org.graphstream:mbox2:1.0")
            library("graphstream.pherd", "org.graphstream:pherd:1.0")
            library("graphstream.util", "org.graphstream:util:1.0")

            library("jung-graph-impl", "net.sf.jung:jung-graph-impl:2.0.1")
            library("jung-api", "net.sf.jung:jung-api:2.0.1")
            library("jung-visualization", "net.sf.jung:jung-visualization:2.0.1")
            library("jung-io", "net.sf.jung:jung-io:2.0.1")
            library("jung-algorithms", "net.sf.jung:jung-algorithms:2.0.1")

            library("googlecode.java.diff.utils", "com.googlecode.java-diff-utils:diffutils:1.3.0")
            library("googlecode.gson", "com.google.code.gson:gson:2.8.9")
            library("google.collections", "com.google.collections:google-collections:1.0")

            library("google.guava", "com.google.guava:guava:23.0") // Update if and when org.jsr-305 is removed

            library("collections.generic", "net.sourceforge.collections:collections-generic:4.01")
            library("simpleframework.simple-xml", "org.simpleframework:simple-xml:2.7.1")
            library("jgoodies-common", "com.jgoodies:jgoodies-common:1.8.1")
            library("jgoodies.looks", "com.jgoodies:looks:2.2.2")
            library("sciss.jsyntaxpane", "de.sciss:jsyntaxpane:1.0.0")
            library("jopt-simple", "net.sf.jopt-simple:jopt-simple:5.0.4")
            library("apache-poi", "org.apache.poi:poi:5.2.2")
            library("ximpleware.vtd-xml", "com.ximpleware:vtd-xml:2.13.4")
            library("hamcrest-all", "org.hamcrest:hamcrest-all:1.3")
            library("scala-library", "org.scala-lang:scala-library:2.13.8")
            library("lookandfeel.seaglass", "com.seaglasslookandfeel:seaglasslookandfeel:0.2.1")
            library("byte-buddy-dep", "net.bytebuddy:byte-buddy-dep:1.12.6")
            library("ow2.asm-tree", "org.ow2.asm:asm-tree:9.2")
            library(
                "fasterxml.jackson-dataformat-yaml",
                "com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.13.1"
            )
            library("fasterxml.jackson-databind", "com.fasterxml.jackson.core:jackson-databind:2.13.1")
            library("io.netty-all", "io.netty:netty-all:4.1.72.Final")
            library("javassist", "javassist:javassist:3.12.1.GA")
            library("jcabi-manifests", "com.jcabi:jcabi-manifests:1.1")
            library("esotericsoftware.kryo", "com.esotericsoftware:kryo:4.0.0")
            library("ibm.icu.icu4j", "com.ibm.icu:icu4j:58.2")
            library("jakarta.xml.bind.api", "jakarta.xml.bind:jakarta.xml.bind-api:3.0.1")
        }
    }
}

include("swtuinterface")
