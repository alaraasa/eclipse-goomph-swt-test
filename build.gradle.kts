plugins {
    base
    id("java")
    kotlin("jvm") version "1.7.10"
    id("com.diffplug.osgi.bndmanifest") version "3.37.1" apply false
    id("com.diffplug.eclipse.mavencentral") version "3.37.1" apply false
    id("com.diffplug.p2.asmaven") version "3.37.1" apply false
}

// Eclipse 4.9: https://www.eclipse.org/downloads/packages/release/2018-09/r
// Eclipse 4.9 DL: https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2018-09/R/eclipse-java-2018-09-win32-x86_64.zip
// Eclipse 4.9 updatesite: https://download.eclipse.org/eclipse/updates/4.9/
//
// Installation:
// Copy Eclipse 4.9 to a place, then cd into it
// Run (make sure to change {PATH TO THIS PROJECT}!):
// eclipsec.exe -nosplash -clean -application org.eclipse.equinox.p2.director -consolelog -repository file:///{PATH TO THIS PROJECT}/swtuinterface/build/updatesite/ -installIU org.example.swtuinterface.feature.group
group = "org.example"
version = "1.0"

repositories {
    mavenCentral()
}

buildscript {
    dependencies {
        classpath("org.standardout:bnd-platform:1.9.0")
    }
}

allprojects {
    dependencies {
        repositories {
            mavenLocal()
            gradlePluginPortal()
            mavenCentral()
        }
    }
}
subprojects.forEach { subProject ->
    subProject.apply(plugin = "java")
    subProject.apply(plugin = "org.jetbrains.kotlin.jvm")
    subProject.apply(plugin = "maven-publish")
    subProject.apply(plugin = "com.diffplug.osgi.bndmanifest")
}
